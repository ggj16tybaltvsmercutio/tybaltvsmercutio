﻿using UnityEngine;
using System.Collections;

public class Lover : MonoBehaviour {
	public AudioSource swoonSound;

    public float baseSpeed;
	public float excitedSpeed;
    public bool _isDead = false;
	public PathOfLove path;
	[HideInInspector] public int curPathIdx;
	public float excitmentDuration;
	float timer;

    //Audio Vars
    public AudioClip[] loverSwoon;
    public AudioClip loverDeath;
    
	float angle;
	new Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if(curPathIdx == -1) {
			transform.rotation = Quaternion.AngleAxis(75, Vector3.right);
			return;
		}

		// if we're close enough to this node, aim for the next.
		float sqrDist = (path.path[curPathIdx].position - transform.position).sqrMagnitude;
		if(sqrDist < 2f) {
			curPathIdx++;
			if(curPathIdx >= path.path.Length) {
				//all done!
				curPathIdx = -1;
				rigidbody.velocity = Vector3.zero;
				swoonSound.clip = loverDeath;
				swoonSound.Play();
				return;
			}
		}

		Vector3 direction = path.path[curPathIdx].position - transform.position;
		direction.y = 0f;
		direction.Normalize();
		rigidbody.velocity = direction * (timer > 0 ? excitedSpeed : baseSpeed);

		angle = Mathf.Lerp(angle, Mathf.Atan2(direction.y, Mathf.Abs(direction.x)), .1f);
		Quaternion rotation = Camera.main.transform.rotation;
		rotation *= Quaternion.AngleAxis(Mathf.Clamp(Mathf.Rad2Deg * angle, -60, 60), Vector3.up);
		transform.rotation = rotation;

		if(timer > 0f) {
			timer -= Time.deltaTime;
		}
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Player") {
			Player player = collision.gameObject.GetComponent<Player>();
			if(player != null && player.carriedLetter) {
				GameObject letter = player.carriedLetter;
				player.DropLetter(Vector2.zero);
				ReadLetter(letter);
			}
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "love letter") {
			ReadLetter(other.gameObject);
		}
	}

	void ReadLetter(GameObject letter) {

		GameObject.Destroy(letter);
        int audioSample = Random.Range(0, loverSwoon.Length);
        swoonSound.clip = loverSwoon[audioSample];
        swoonSound.Play();
		timer = excitmentDuration;
	}
}
