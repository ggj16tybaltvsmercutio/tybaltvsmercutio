﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Collections;

public class GameOverMenu : MonoBehaviour {

    public AudioMixerSnapshot TrapIn;
    public AudioMixerSnapshot DiscoIn;
    public AudioSource MenuSound;
    public GameObject Instructions;
	float timer;

	void Start() {
		timer = 0f;
	}

	void Update() {
		timer += Time.deltaTime;
		if (timer >= .5f && Input.anyKeyDown) {
			ReturnToMainMenu();
		}
	}

	public void RestartGame() {
        DiscoIn.TransitionTo(0.1f);
        MenuSound.Play();
        Application.LoadLevel(1);
        //HideInstr();
    }
    public void ReturnToMainMenu() {
		Application.LoadLevel(0);
	}

    public void ShowInstr()
    {
        Instructions.SetActive(true);
    }
    public void HideInstr()
    {
        Instructions.SetActive(false);
    }
}
