﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUD : MonoBehaviour {
	public GameObject romeoImage;
	public GameObject julietImage;
	public Lover romeo;
	public Lover juliet;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = new Vector3();
		pos.x = juliet.curPathIdx * 60f - 360f;
		julietImage.transform.localPosition = pos;
		pos.x = romeo.curPathIdx * 60f - 360f;
		pos.y = 25;
		romeoImage.transform.localPosition = pos;

	}
}
