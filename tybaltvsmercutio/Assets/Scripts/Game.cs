﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Collections;

public class Game : MonoBehaviour {
	public GameObject letterPrefab;
	public Transform neutralSpawn;
	public Transform[] julietBehindSpawns;
	public Transform[] romeoBehindSpawns;
	public Lover romeo;
	public Lover juliet;
	public float letterCooldown;
	public Canvas gameOverMenu;
	public Text gameOverText;
	public Color red;
	public Color violet;

	float timer;

    public AudioMixerSnapshot TrapIn;
    public AudioMixerSnapshot DiscoIn;

	// Use this for initialization
	void Start () {
        DiscoIn.TransitionTo(0.1f);
	}

	// Update is called once per frame
	void Update () {

		if (romeo.curPathIdx == -1 || juliet.curPathIdx == -1) {
			if (romeo.curPathIdx == -1 && juliet.curPathIdx != -1) {
				gameOverText.text = "Tybalt (red) Wins!";
				gameOverText.color = red;
			} else if (romeo.curPathIdx != -1 && juliet.curPathIdx == -1) {
				gameOverText.text = "Mercutio (violet) Wins!";
				gameOverText.color = violet;
			}
			Debug.Log("Game Over!");
            TrapIn.TransitionTo(0.1f);
            gameOverMenu.gameObject.SetActive(true);
		}

		timer -= Time.deltaTime;
		if (timer <= 0f) {
			timer = letterCooldown;
			Vector3 pos;


			if (romeo.curPathIdx == juliet.curPathIdx) {
				pos = neutralSpawn.position;
			} else if (romeo.curPathIdx == juliet.curPathIdx - 1) {
				pos = julietBehindSpawns[0].position;
			} else if (romeo.curPathIdx > juliet.curPathIdx) {
				pos = julietBehindSpawns[1].position;
			} else if (juliet.curPathIdx == romeo.curPathIdx - 1) {
				pos = romeoBehindSpawns[0].position;
			} else {
				pos = romeoBehindSpawns[1].position;
			}

			GameObject letter = GameObject.Instantiate(letterPrefab, pos, Quaternion.identity) as GameObject;
		}
	}	
}
