﻿using UnityEngine;
using System.Collections;

public class Sword : MonoBehaviour {
	[HideInInspector] public float jabTimer;
	[HideInInspector] public float parryTimer;
	public Player player;
	public Sprite jabSprite;
	public Sprite parrySprite;
	SpriteRenderer spriteRenderer;
	new Collider collider;
	[HideInInspector] public bool recovered;

	// Use this for initialization
	void Start() {
		collider = GetComponent<Collider>();
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	void Update() {

		// check for end of parry/jab:
		if (parryTimer > 0f) {
			parryTimer -= Time.deltaTime;
		}
		if (jabTimer > 0f) {
			jabTimer -= Time.deltaTime;
		}
		recovered |= (jabTimer <= 0f && parryTimer <= 0f);
		if (recovered) {
			collider.enabled = false;
			collider.isTrigger = false;
			spriteRenderer.enabled = false;
		}
		recovered = false;
	}

	public void Parry() {
		parryTimer = .5f;
		//collider.enabled = true;
		spriteRenderer.enabled = true;
		spriteRenderer.sprite = parrySprite;

	}
	public void Jab() {
		jabTimer = .5f;
		collider.enabled = true;
		collider.isTrigger = true;
		spriteRenderer.enabled = true;
		spriteRenderer.sprite = jabSprite;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "sword") {
			Sword otherSword = other.gameObject.GetComponent<Sword>();
			if (parryTimer > 0f && otherSword.jabTimer > 0f) {
				player.DidParry();
				otherSword.player.GotParried();
			}
		}
	}

}
