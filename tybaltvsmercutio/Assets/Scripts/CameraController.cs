﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public Player player1;
	public Player player2;
	public float fixedDistance;
	public float twistyDistance;
	public float maxRotation;
	public float distanceMuliplier;
	public float minSize;
	public float maxSize;
	public float height;
	public float minHeight;

	new Camera camera;


	// Use this for initialization
	void Start () {
		camera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		float playerDistance = (player1.transform.position - player2.transform.position).magnitude;
        Vector3 avg = (player1.transform.position + player2.transform.position) * .5f;
		Vector3 offset = new Vector3();
		offset.x = player2.transform.position.z - player1.transform.position.z;
		offset.z = player1.transform.position.x - player2.transform.position.x;
		offset.Normalize();
		offset *= twistyDistance / Mathf.Max(12f, playerDistance);

		Vector3 pos = avg + offset + fixedDistance * Vector3.back;
		pos.y = height;
		float closeThreshold = minSize / distanceMuliplier;
		if(playerDistance < closeThreshold) {
			offset *= closeThreshold / playerDistance;
			pos.y = Mathf.Max(minHeight, height * playerDistance / closeThreshold);
        }
		transform.position = pos;
		transform.LookAt(avg);
		camera.orthographicSize = Mathf.Clamp(playerDistance * distanceMuliplier, minSize, maxSize);
	}
}
