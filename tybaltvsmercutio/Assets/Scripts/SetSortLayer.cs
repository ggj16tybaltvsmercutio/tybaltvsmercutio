﻿using UnityEngine;
using System.Collections;

public class SetSortLayer : MonoBehaviour {
	public string sortLayer;
	public int orderInLayer;

	// Use this for initialization
	void Start () {
		Renderer[] Renderers = GetComponentsInChildren<Renderer>();
		foreach(Renderer renderer in Renderers) {
			renderer.sortingLayerName = sortLayer;
			renderer.sortingOrder = orderInLayer;
		}
	}
}
