﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Collections;

public class MainMenuScript : MonoBehaviour {
	float timer;
	bool instructionsUp;

	void Start() {
		timer = 0f;
		instructionsUp = false;
	}

	void Update() {
		timer += Time.deltaTime;
		if(timer >= .5f && Input.anyKeyDown) {
			if (instructionsUp) {
				StartGame();
			} else {
				ShowHUD();
				instructionsUp = true;
			}
		}
	}

    public GameObject Instructions;

    public AudioMixerSnapshot DiscoIn;
    public AudioSource MenuSound;
    public AudioClip pageTurn, startGame;


	public void StartGame() {
        Debug.Log("StartGame?");
        MenuSound.clip = startGame;
        MenuSound.Play();
        DiscoIn.TransitionTo(0.1f);

        Application.LoadLevel(1);
        //HideHUD();
    }
    public void ExitGame() {
        Application.Quit();
    }
    public void ShowHUD()  {
        MenuSound.clip = pageTurn;
        MenuSound.Play();
        Instructions.SetActive(true);
    }
    public void HideHUD () {
        Instructions.SetActive(false);
    }
    
}
