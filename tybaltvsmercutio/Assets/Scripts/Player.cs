﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public int playerNum;
	public float speed;
	public Sword sword;

    //Audio Vars
    public AudioSource walkSound;

    public AudioSource fightSound;
    public AudioClip[] swordSwings;
    public AudioClip[] parrySound;

    public AudioSource swordHit;
    public AudioClip paperPickup;
    public AudioClip paperDrop;
    public AudioClip[] playerParry;
    public AudioClip[] playerWound;
	public GameObject bloodPrefab;

	string inputHorz;
	string inputVert;
	string inputJab;
	string inputParry;
	float angle;
	float noCarryTimer;
	float woundedTimer;
	[HideInInspector] public float gotParriedTimer;
	[HideInInspector] public float didParryTimer;
	Collider swordCollider;
	new Rigidbody rigidbody;
	[HideInInspector] public GameObject carriedLetter;
	[HideInInspector] public SpriteRenderer sprite;
	Animator anim;


	// Use this for initialization
	void Start () {
		// set which inputs to look for:
		switch (playerNum) {
			case 1:
				inputHorz = "p1 horz";
				inputVert = "p1 vert";
				inputJab = "p1 jab";
				inputParry = "p1 parry";
				break;
			case 2:
				inputHorz = "p2 horz";
				inputVert = "p2 vert";
				inputJab = "p2 jab";
				inputParry = "p2 parry";
				break;
		}

		rigidbody = GetComponent<Rigidbody>();
		anim = GetComponent<Animator>();
		sprite = GetComponentInChildren<SpriteRenderer>();
		Transform sword = transform.Find("sword");
		swordCollider = sword.GetComponent<Collider>();
	}

	// Update is called once per frame
	void Update() {
		Vector3 vel = new Vector3();

		// parried stun:
		if(gotParriedTimer > 0f) {
			gotParriedTimer -= Time.deltaTime;
			rigidbody.velocity = Vector3.zero;
			transform.rotation = Quaternion.AngleAxis(75, Vector3.right);
			sprite.enabled = true;
			return;
		}

		// normalize input:
		Vector2 input = new Vector2(Input.GetAxis(inputHorz), Input.GetAxis(inputVert));
		if (Mathf.Abs(input.x) < .5f) {
			input.x = 0f;
		}
		if (Mathf.Abs(input.y) < .5f) {
			input.y = 0f;
		}
		input.Normalize();

		// translate input into camera coordinates:
		Transform cameraTransform = Camera.main.transform;
		Vector3 forward = cameraTransform.forward;
		forward.y = 0f;
		forward.Normalize();
		Vector3 right = cameraTransform.right;
		right.y = 0f;
		right.Normalize();
		float curSpeed = speed;
		if(carriedLetter != null) {
			curSpeed *= .8f;
		}
		if(woundedTimer > 0f) {
			curSpeed *= .6f;
		}
		vel += right * input.x * curSpeed;
		vel -= forward * input.y * curSpeed;
		vel.y = rigidbody.velocity.y;   //maintain velocity from e.g. gravity.

		// walking audio
		if (!walkSound.isPlaying && (input.x != 0f || input.y != 0f)) {
			walkSound.Play();
		} else if (walkSound.isPlaying && input.x == 0f && input.y == 0f) {
			walkSound.Pause();
		}

		//apply input to velocity:
		rigidbody.velocity = vel;

		// turn to face camera, modified by walking direction:
		angle = Mathf.Lerp(angle, Mathf.Atan2(input.y, Mathf.Abs(input.x)), .1f);
		Quaternion rotation = Camera.main.transform.rotation;
		rotation *= Quaternion.AngleAxis(Mathf.Clamp(Mathf.Rad2Deg * angle, -60, 60), Vector3.up);
		transform.rotation = rotation;
		if (input != Vector2.zero) {
			Vector3 scale = transform.localScale;
			if(input.x < 0f) {
				scale.x = -Mathf.Abs(scale.x);
			} else {
				scale.x = Mathf.Abs(scale.x);
			}
			transform.localScale = scale;
		}

		// update a carried letter:
		if (carriedLetter) {
			Vector3 pos = transform.position;
			pos.z += .1f;
			carriedLetter.transform.position = pos;
			carriedLetter.transform.rotation = transform.rotation;
			if (Input.GetButtonDown(inputJab)) {
				DropLetter(input * 2f);
			}
		}

		if (noCarryTimer > 0f) {
			noCarryTimer -= Time.deltaTime;
		}

		//update jab/parry:
		if (Input.GetButtonDown(inputJab) && sword.jabTimer <= 0f && sword.parryTimer <= 0f) {
			anim.SetTrigger("jab");
			swordCollider.enabled = true;
			sword.Jab();
			int audioSample = Random.Range(0, swordSwings.Length);
			fightSound.clip = swordSwings[audioSample];
			fightSound.Play();
        } else if (Input.GetButtonDown(inputParry) && sword.jabTimer <= 0f && sword.parryTimer <= 0f) {
			anim.SetTrigger("parry");
            int audioSample = Random.Range(0, parrySound.Length);
            fightSound.clip = parrySound[audioSample];
            fightSound.Play();
			swordCollider.enabled = true;
			sword.Parry();
		}

		// update timers:
		if(didParryTimer > 0f) {
			didParryTimer -= Time.deltaTime;
		}
		if(gotParriedTimer > 0f) {
			gotParriedTimer -= Time.deltaTime;
		}
		if (woundedTimer > 0) {
			woundedTimer -= Time.deltaTime;
			sprite.enabled = (woundedTimer % .1f > .05f);
		} else {
			sprite.enabled = true;
		}
	}

	void OnTriggerStay(Collider other) {
		if (other.tag == "love letter" && noCarryTimer <= 0f && carriedLetter == null) {
            fightSound.clip = paperPickup;
            fightSound.Play();
			carriedLetter = other.gameObject;
			other.enabled = false;
		}else if(other.tag == "sword" && sword.parryTimer <= 0f && didParryTimer <= 0f) {
			Sword otherSword = other.GetComponent<Sword>();
			if (otherSword.player != this && otherSword.jabTimer > 0) {
				GotStabbed();
			}
		} else if (other.tag == "sword" && sword.parryTimer > 0f && ( woundedTimer < 4f || woundedTimer >= 4.9f)) {
			Sword otherSword = other.GetComponent<Sword>();
			if (otherSword.player != this && otherSword.jabTimer > 0) {
				DidParry();
				otherSword.player.GotParried();
			}
		}
	}

	public void GotStabbed() {
		if (woundedTimer <= 0f && sword.parryTimer <= 0f) {
			woundedTimer = 5f;
            int audioSample = Random.Range(0, playerWound.Length);
            fightSound.clip = playerWound[audioSample];
            fightSound.Play();
			GameObject blood = GameObject.Instantiate(bloodPrefab, transform.position, transform.rotation) as GameObject;
			GameObject.Destroy(blood, 2f);
			if (carriedLetter != null) {
				DropLetter(Vector2.zero);
			}
		}
	}

	public void GotParried() {
		gotParriedTimer = 2f;
		sword.recovered = true;
	}

	public void DidParry() {
		int audioSample = Random.Range(0, playerParry.Length);
		fightSound.clip = playerParry[audioSample];
		fightSound.Play();
		didParryTimer = 1f;
		woundedTimer = 0f;
	}
	
	public void DropLetter(Vector2 dir) {
		// drop a carried letter:
		carriedLetter.GetComponent<Collider>().enabled = true;
        fightSound.clip = paperDrop;
        fightSound.Play();
		carriedLetter.transform.position = transform.position + new Vector3(dir.x, 0f, -dir.y);
		carriedLetter = null;
		noCarryTimer = 2f;
	}

}
